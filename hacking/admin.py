from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.HackAttempt)


@admin.register(models.HackProgress)
class SkillBought(admin.ModelAdmin):
    search_fields = [
        'hacker__first_name',
        'hacker__last_name',
        'target__first_name',
        'target__last_name',
        'completed',
    ]

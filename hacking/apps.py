from django.apps import AppConfig


class HackingConfig(AppConfig):
    name = 'hacking'

    def ready(self):
        from hacking.updater import start
        start()
        import hacking.signals

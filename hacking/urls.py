from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('overview/', views.hack_overview, name='hack_overview'),
    path('overview/<compromised>/<cooldown>', views.hack_overview, name='hack_overview'),
    path('swipe_overview', views.swipe_overview, name='swipe_overview'),
    path('attempts/', views.hack_attempts, name='hack_attempts'),
    path('swipe/<target_id>', views.swipe_target, name='swipe_target'),
    path('hack_target_credits/<target_id>', views.hack_target_credits, name='hack_target'),
    path('hack_target_mail/<target_id>', views.hack_target_mail, name='hack_target_mail'),
    path('stealable_emails/', views.stealable_emails, name='stealable_emails'),
    path('steal_email/<message_id>/<attempt_id>', views.steal_email, name='steal_email'),
]

from django import forms


class HackTargetForm(forms.Form):
    security_token = forms.CharField(max_length=70, widget=forms.HiddenInput)
    char_id = forms.IntegerField(widget=forms.widgets.HiddenInput)

class HackChooseExtraRisk(forms.Form):
    extra_stolen_things = forms.IntegerField()


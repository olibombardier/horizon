from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Sector)
admin.site.register(Tick)

@admin.register(Assistance)
class Assistance(admin.ModelAdmin):
    search_fields = ['character__first_name', 'character__last_name']

@admin.register(Action)
class Action(admin.ModelAdmin):
    search_fields = ['character__first_name', 'character__last_name']

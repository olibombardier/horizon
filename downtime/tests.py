from datetime import datetime

from character.management.commands.parse import run as parserun
from character.models import Character, Corporation, Origin, Skill, SkillBought
from django.test import TestCase
from downtime.management.commands.resolve_downtime import resolve_sectors
from downtime.management.commands.start_downtime import pay_influence_everyone, pay_innovation_everyone
from downtime.models import Action, Assistance, Sector, Tick
from larpdate.models import LarpDate


# Create your tests here.
class DowntimeTestCase(TestCase):
    def setUp(self):
        origins = [
            'Aristocratie corporative',
            'Une toile de contacts',
        ]
        for iorigin in origins:
            origin = Origin(name=iorigin, description='', bonus='', malus='')
            origin.save()

        corp = Corporation(name='testcorp')
        corp.save()
        self.char = Character(
            origin=origin,
            corporation=corp,
            first_name='test',
            last_name='testy',
        )
        self.char.save()

        self.char2 = Character(
            origin=origin,
            corporation=corp,
            first_name='test2',
            last_name='testy2',
        )
        self.char2.save()


        larpdate = LarpDate(out_date=datetime.today(), in_date=datetime.today())
        larpdate.save()

        tick = Tick(larp=larpdate)
        tick.save()

        self.sector = Sector(name='test', base_vd=0)
        self.sector.save()
        parserun(ask=False)

    def test_char_innovation(self):
        self.char.current_larp_active=True
        self.char.save()

        self.assertEqual(self.char.passive_innovation, 0)

        pay_innovation_everyone()
        self.char.refresh_from_db()
        self.assertEqual(self.char.passive_innovation, 0)

        sb = SkillBought(character=self.char, skill=Skill.objects.get(name="Armurerie - Innovation"))
        sb.save()

        pay_innovation_everyone()
        self.char.refresh_from_db()
        self.assertEqual(self.char.passive_innovation, 1)

    def test_char_influence(self):
        self.char.current_larp_active=True
        self.char.save()

        self.assertEqual(self.char.current_influence, 0)
        self.assertEqual(self.char.effective_influence, 0)

        skills = [
            'Contact corporatif',
            'Réseau corporatif',
            'Relations corporatives',
            'Alliés clandestins',
        ]

        for iskill in skills:
            skill = Skill.objects.filter(name__icontains=iskill).first()
            sb = SkillBought(character=self.char, skill=skill)
            sb.save()

        pay_influence_everyone()
        self.char.refresh_from_db()

        self.assertEqual(self.char.current_influence, 5)
        self.assertEqual(self.char.effective_influence, 5)

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=5,
            sector=self.sector
        )
        action.save()

        self.char.refresh_from_db()
        self.assertEqual(self.char.effective_influence,0)

    def test_sector_conquest_win(self):
        self.assertIsNone(self.sector.controlling_character)

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()


        resolve_sectors()
        self.sector.refresh_from_db()

        self.assertEqual(self.sector.controlling_character, self.char)

    def test_sector_conquest_draw(self):
        self.assertIsNone(self.sector.controlling_character)

        self.sector.controlling_character = self.char2
        self.sector.save()

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()
        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()


        resolve_sectors()
        self.sector.refresh_from_db()

        self.assertIsNone(self.sector.controlling_character)

    def test_sector_defended(self):
        self.assertIsNone(self.sector.controlling_character)

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.DEFENCE,
            influence_invested=20,
            sector=self.sector
        )
        action.save()


        resolve_sectors()
        self.sector.refresh_from_db()

        self.assertIsNone(self.sector.controlling_character)


    def test_clandestine_allies(self):
        self.assertIsNone(self.sector.controlling_character)

        self.sector.previously_controlled_by.add(self.char)
        self.sector.save()

        clandestine_skill = Skill.objects.get(name='Alliés clandestins')

        sb = SkillBought(
            character=self.char,
            skill=clandestine_skill
        )
        sb.save()

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.DEFENCE,
            influence_invested=11,
            sector=self.sector
        )
        action.save()


        resolve_sectors()
        self.sector.refresh_from_db()

        self.assertEqual(self.sector.controlling_character, self.char)

    def test_total_influence(self):
        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()
        self.assertEqual(action.total_influence, 10)

        # Check if asisstances are counted
        assistance = Assistance(action=action, character=self.char, influence_invested=10)
        assistance.save()
        action.refresh_from_db()
        self.assertEqual(action.total_influence, 20)

        # Check if assistance on another action do not collide
        action2 = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action2.save()
        assistance = Assistance(action=action2, character=self.char, influence_invested=10)
        assistance.save()
        action.refresh_from_db()
        self.assertEqual(action.total_influence, 20)

        # Confirm Aristocratie malus works as intended
        self.char.origin = Origin.objects.filter(name='Aristocratie corporative').first()
        self.char.save()
        action.refresh_from_db()
        self.assertEqual(action.total_influence, 19)

        assistance = Assistance(action=action, character=self.char, influence_invested=10)
        assistance.save()
        action.refresh_from_db()
        self.assertEqual(action.total_influence, 29)

    def test_char_downtime_count(self):
        self.assertEqual(self.char.remaining_downtimes,2)

        action = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.CONQUEST,
            influence_invested=10,
            sector=self.sector
        )
        action.save()

        self.char.refresh_from_db()
        self.assertEqual(self.char.remaining_downtimes, 1)

        action2 = Action(
            character=self.char,
            tick=Tick.objects.first(),
            action_type=Action.ASSISTANCE,
            influence_invested=10,
            sector=self.sector
        )
        action2.save()
        assistance = Assistance(
            action=action2,
            character=self.char
        )
        assistance.save()

        self.char.refresh_from_db()
        self.assertEqual(self.char.remaining_downtimes, 1)

        assistance.active_participation = True
        assistance.save()

        self.char.refresh_from_db()
        self.assertEqual(self.char.remaining_downtimes, 0)

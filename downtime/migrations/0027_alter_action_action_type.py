# Generated by Django 4.1.6 on 2023-02-26 00:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('downtime', '0026_action_healed_condition'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='action_type',
            field=models.CharField(choices=[('CQ', 'Conquête'), ('DEF', 'Défense'), ('SAB', 'Sabotage'), ('AS', 'Assistance'), ('OP', 'Opération'), ('CR', "Fabrication d'objet"), ('AUG', "Installation d'augmentations"), ('GEN', "Générer de l'infuence avec une compétence"), ('SH', 'Acheter ou vendre des objets'), ('SEC', 'Utiliser la compétence "Systèmes de sécurité"'), ('MIL', '(Secteur Militaire) Soigner une condition')], default='CQ', max_length=3),
        ),
    ]

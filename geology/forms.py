from django import forms

from geology.models import Pole


class SelectPoleForm(forms.Form):
    security_token = forms.CharField(max_length=70, widget=forms.HiddenInput)
    pole_id = forms.IntegerField(widget=forms.widgets.HiddenInput)


class InstallPoleForm(forms.ModelForm):
    class Meta:
        model = Pole
        fields = ['owner', 'target_mineral']

from django.apps import AppConfig


class GeologyConfig(AppConfig):
    name = 'geology'

    def ready(self):
        import geology.signals

from django.contrib import admin

from geology.models import Mineral, Pole

# Register your models here.
admin.site.register(Mineral)
admin.site.register(Pole)

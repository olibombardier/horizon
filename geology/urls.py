from django.contrib.auth import views as auth_views
from django.urls import path

from geology import views

urlpatterns = [
    path('pole_qr/<pole_id>', views.pole_qr, name='pole_qr'),
    path('overview/', views.geology_overview, name='geology_overview'),
    path('claim_pole/', views.claim_pole, name='claim_pole'),
    path('install_pole/', views.install_pole, name='install_pole'),
    path('collect_pole/', views.collect_pole, name='collect_pole'),
]

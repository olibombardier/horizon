from django.db import models

from character.models import Character


# Create your models here.
class ProgramType(models.Model):
    HACK_OFFENCE = 'HO'
    HACK_DEFENCE = 'HD'
    UTILITY = 'UT' 
    PROGRAM_TYPE_CHOICES = [
        (HACK_OFFENCE, 'Programme Offensif'),
        (UTILITY, 'Utilitaire'),
        (HACK_DEFENCE, 'Programme Défensif')
    ]
    name = models.CharField(max_length=100)
    classification = models.CharField(
        max_length=2,
        choices=PROGRAM_TYPE_CHOICES,
    )
    id_code = models.CharField(max_length=10)

    def __str__(self):
        return f'{self.id_code} - {self.name}'

class InstalledProgram(models.Model):
    target = models.ForeignKey(Character, on_delete=models.CASCADE, related_name='programs_installed')
    installed_by = models.ForeignKey(Character, on_delete=models.PROTECT, related_name='installed_by', null=True)
    program = models.ForeignKey(ProgramType, on_delete=models.PROTECT)
    effect_target = models.ForeignKey(
        Character,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='programs_effect_target'
    )

    def __str__(self):
        return f'{self.target} - {self.program}'

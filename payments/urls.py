from django.urls import path
from . import views

app_name = 'payments'
urlpatterns = [
    path('add/', views.add_payment, name='add'),
    path('list_all/', views.list_all, name='list_all'),
    path('list_pending/', views.list_pending, name='list_pending'),
    path('details/<int:id>/', views.details, name='details'),
    path('login/', views.login_user, name='login'),
    path('update_refund/<int:id>', views.update_refund, name='update_refund'),
    path('change_password/', views.change_password, name='change_password'),
    path('owned/', views.owned_by_user, name='owned_by_user')
]

from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PaymentForm, LoginForm, RefundStatusForm, PasswordChangeForm
from .models import Payment, Category, Committee, Event
from datetime import datetime
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Sum


# Create your views here.
@login_required
def add_payment(request):
    if request.method == 'POST':
        form = PaymentForm(request.POST, request.FILES)
        if form.is_valid():
            p = Payment()
            form.clean()
            p.purchase = form.cleaned_data.get('purchase')
            p.purchase_date = form.cleaned_data.get('purchase_date')
            p.entry_date = datetime.now()
            p.amount = form.cleaned_data.get('amount')
            p.category = form.cleaned_data.get('category')
            p.committee = form.cleaned_data.get('committee')
            p.event = form.cleaned_data.get('event')
            p.type = form.cleaned_data.get('type')
            p.bill = form.cleaned_data.get('bill')
            p.paid_by_onbl = form.cleaned_data.get('paid_by_onbl')
            p.owner = request.user
            if p.paid_by_onbl == True:
                p.is_refunded = True
            p.save()
            return redirect('/payments/list_all/')

    else:
        form = PaymentForm()
        return render(request, 'payments/add.djhtml', {'form': form})


@login_required
def list_all(request):
    context = {'payments': Payment.objects.all()}

    return render(request, 'payments/list.djhtml', context)

@login_required
def owned_by_user(request):
    if request.method == 'POST' and request.user.has_perm('payments.update_refund_status'):
        user_id = request.POST['user_id']
        to_refund = Payment.objects.filter(owner=user_id).filter(is_refunded=False)
        for payment in to_refund:
            payment.is_refunded = True
            payment.refunded_date = datetime.now()
            payment.save()


    context = {
        'owned': User.objects.filter(payment__is_refunded=False).annotate(total=Sum('payment__amount'))
    }

    return render(request, 'payments/owned_by_user.djhtml', context)

@login_required
def list_pending(request):
    context = {
        'payments': Payment.objects.filter(is_refunded=False).order_by('owner'),
    }

    return render(request, 'payments/list.djhtml', context)


@login_required
def details(request, id):
    try:
        payment = Payment.objects.get(id=id)
        form = RefundStatusForm(initial={'status':payment.is_refunded})

        if payment.bill.url[-3:] == 'pdf':
            is_pdf = True
        else:
            is_pdf = False

        context = {
            'payment': payment,
            'form': form,
            'is_pdf': is_pdf
        }
    except:
        context = {}

    return render(request, 'payments/details.djhtml', context)

@login_required
def update_refund(request, id):
    if request.method == 'POST' and request.user.has_perm('payments.update_refund_status'):
        form = RefundStatusForm(request.POST)
        if form.is_valid():
            p = Payment.objects.get(id=id)
            form.clean()
            p.is_refunded = form.cleaned_data.get('status')
            if p.is_refunded:
                p.refunded_date = datetime.now()
            p.save()
            return redirect('/payments/details/' + str(id) + '/')

    return redirect('/payments/details/' + str(id) + '/')

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.clean()
            pw = form.cleaned_data.get('password')
            pwc = form.cleaned_data.get('password_confirmation')
            if pw == pwc:
                print("DONE!")
                user = request.user
                user.set_password(pw)
                user.save()
                return redirect('/payments/add/')
            else:
                form = PasswordChangeForm()
                return render(request, 'payments/password_change.djhtml', {'no_match': True, 'form': form})
    else:
        form = PasswordChangeForm()
        return render(request, 'payments/password_change.djhtml', {'form': form})

def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            form.clean()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/payments/add/')

    form = LoginForm()
    return render(request, 'payments/login.djhtml', {'form': form})

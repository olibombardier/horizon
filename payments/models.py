from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


# Create your models here.
class Category(models.Model):
    category_name = models.CharField(max_length=50)
    is_visible = models.BooleanField(default=True)

    def __str__(self):
        return str(self.category_name)


class Committee(models.Model):
    committee_name = models.CharField(max_length=50)

    def __str__(self):
        return str(self.committee_name)

class Event(models.Model):
    event_name = models.CharField(max_length=50)
    is_visible = models.BooleanField(default=True)

    def __str__(self):
        return self.event_name


class Payment(models.Model):
    PURCHASE = 1
    INCOME = 2
    PAYMENT_TYPE_CHOICES = (
        (PURCHASE, 'Achat'),
        (INCOME, 'Revenus')
    )
    purchase_date = models.DateTimeField()
    entry_date = models.DateTimeField(default=datetime.now)
    purchase = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=50, decimal_places=2)
    bill = models.FileField(upload_to="bills/")
    is_refunded = models.BooleanField(default=False)
    refunded_date = models.DateTimeField(blank=True, null=True)
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    committee = models.ForeignKey(Committee, on_delete=models.PROTECT)
    type = models.SmallIntegerField(choices=PAYMENT_TYPE_CHOICES)
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    paid_by_onbl = models.BooleanField(default=False)

    def __str__(self):
        date = self.purchase_date.strftime('%Y-%m-%d')
        name = self.owner.get_full_name()
        if name == "":
            name = self.owner.__str__()

        return date + " - " + str(self.amount) + " - " + name + " - " + self.purchase

    class Meta:
        permissions = (("update_refund_status",
                        "Can update the status of a refund"), )



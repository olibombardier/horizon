from django import forms
from .models import Payment, Category, Committee, Event
from django.db.utils import OperationalError, ProgrammingError
import datetime

class PaymentForm(forms.Form):
    try:
        visible_categories = Category.objects.filter(is_visible=True)
        all_commities = Committee = Committee.objects.all()

        visible_events = Event.objects.filter(is_visible=True)

        purchase = forms.CharField(max_length=100, label="Achat")
        purchase_date = forms.DateField(initial=datetime.date.today, label="Date de la transaction")
        amount = forms.DecimalField(max_digits=50, decimal_places=2, label="Montant")
        type = forms.ChoiceField(choices=Payment.PAYMENT_TYPE_CHOICES, widget=forms.RadioSelect())
        category = forms.ModelChoiceField(queryset=visible_categories, label="Catégorie", empty_label=None)
        committee = forms.ModelChoiceField(queryset=all_commities, label="Comité", empty_label=None)
        event = forms.ModelChoiceField(queryset=visible_events, label="Évènement", empty_label=None)
        paid_by_onbl = forms.BooleanField(required=False, label="Payé avec l'argent de COE (Aucun remboursement requis)")
        bill = forms.FileField(label="Facture")
    except (OperationalError, ProgrammingError):
        pass



class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, label='login')
    password = forms.CharField(widget=forms.PasswordInput())

class RefundStatusForm(forms.Form):
    status = forms.BooleanField(widget=forms.CheckboxInput, label='Remboursé', required=False)

class PasswordChangeForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput())
    password_confirmation = forms.CharField(widget=forms.PasswordInput())

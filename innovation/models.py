from django.db import models

from character.models import Character, Skill

# Create your models here.


class Source(models.Model):
    out_name = models.CharField(max_length=100)
    in_name = models.CharField(max_length=100, default='Échantillon de source sans nom', null=True, blank=True)
    soft_cap = models.IntegerField(default=3)
    associated_skill = models.ForeignKey(Skill, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.in_name} ({self.out_name}) [{self.associated_skill.name}]'

class Innovation(models.Model):
    owner = models.ForeignKey(Character, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.PROTECT)

    point_gained = models.IntegerField(default=0)
    rp_points_gained = models.IntegerField(default=0)

    points_spent = models.IntegerField(default=0)

    rp_points_notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.owner.__str__() + ' - ' + self.source.__str__()

    @property
    def current_points(self):
        return self.point_gained + self.rp_points_gained - self.points_spent

from django.db.models.signals import post_save
from django.dispatch import receiver

from innovation.models import Innovation


@receiver(post_save, sender=Innovation)
def ensure_alert_has_execute_time(sender, instance, created, raw, using, update_fields, **kwargs):
    if instance.point_gained > instance.source.soft_cap:
        instance.point_gained = instance.source.soft_cap
        instance.save()

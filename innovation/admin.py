from django.contrib import admin

from innovation.models import Innovation, Source

# Register your models here.
admin.site.register(Innovation)
admin.site.register(Source)

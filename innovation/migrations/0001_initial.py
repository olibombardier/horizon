# Generated by Django 2.2.4 on 2019-08-27 03:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('character', '0014_auto_20190826_2038'),
    ]

    operations = [
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('out_name', models.CharField(max_length=100)),
                ('in_name', models.CharField(blank=True, default='Échantillon de source sans nom', max_length=100, null=True)),
                ('soft_cap', models.IntegerField(default=3)),
                ('associated_skill', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='character.Skill')),
            ],
        ),
        migrations.CreateModel(
            name='Innovation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('point_gained', models.IntegerField(default=0)),
                ('rp_points_gained', models.IntegerField(default=0)),
                ('points_spent', models.IntegerField(default=0)),
                ('rp_points_notes', models.TextField(blank=True, null=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='character.Character')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='innovation.Source')),
            ],
        ),
    ]

from django.apps import AppConfig


class ExtraMailConfig(AppConfig):
    name = 'extra_mail'

    def ready(self):
        import extra_mail.signals

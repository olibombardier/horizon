# Generated by Django 4.1.6 on 2023-02-13 12:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('character', '0028_character_is_alterego_character_parent_char_and_more'),
        ('craft', '0012_craftrecord_number_of_items'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='royalties_percent',
            field=models.PositiveSmallIntegerField(default=5),
        ),
        migrations.AddField(
            model_name='recipe',
            name='royalties_target',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='character.character'),
        ),
    ]

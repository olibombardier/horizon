# Generated by Django 4.1.6 on 2023-02-13 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('craft', '0013_recipe_royalties_percent_recipe_royalties_target'),
    ]

    operations = [
        migrations.AddField(
            model_name='craftrecord',
            name='extra_spend',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

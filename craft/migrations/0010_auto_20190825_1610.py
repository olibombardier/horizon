# Generated by Django 2.2.4 on 2019-08-25 20:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('craft', '0009_craftrecord_timestamp'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipecategory',
            options={'ordering': ['name']},
        ),
    ]

# Generated by Django 2.2.4 on 2020-03-15 05:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('craft', '0011_auto_20190825_1738'),
    ]

    operations = [
        migrations.AddField(
            model_name='craftrecord',
            name='number_of_items',
            field=models.IntegerField(default=1),
        ),
    ]

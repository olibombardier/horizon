from django.apps import AppConfig


class AnimToolsConfig(AppConfig):
    name = 'anim_tools'

FROM $BASE_IMG

RUN mkdir -p ./horizon
ADD . ./horizon/

WORKDIR ./horizon

RUN python manage.py collectstatic

EXPOSE 8010
STOPSIGNAL SIGTERM
CMD ["./deploy_tools/start-server.sh"]
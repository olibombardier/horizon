from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from character.utils import get_character


# Create your views here.
@login_required
def list_inventory(request):
    char = get_character(request)

    inventory = char.inventory_set.all()
    context = {}
    context['inventory'] = inventory
    context['character'] = char

    return render(request, 'inventory/overview.djhtml', context)

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver

from character.models import Character


# Create your models here.
class Item(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.name

class Inventory(models.Model):
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    count = models.PositiveIntegerField()
    character = models.ForeignKey(Character, on_delete=models.CASCADE)

    def __str__(self):
        return self.character.full_name + ' - ' + self.item.name + ' ( ' + str(self.count) + ' )'

class Trade(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    amount = models.PositiveIntegerField()
    source = models.ForeignKey(Inventory, on_delete=models.PROTECT, related_name='source')
    destination = models.ForeignKey(Inventory, on_delete=models.PROTECT, related_name='destination')

def get_character_inventory_for_item(char, item):
    return char.inventory_set.filter(item=item).first()

def add_to_inventory(char, item, amount_to_add):
    inventory = get_character_inventory_for_item(char, item)

    if inventory is None:
        inventory = Inventory(item=item, count=0, character=char)

    if inventory.count + amount_to_add >= 0:
        inventory.count += amount_to_add
    else:
        raise ArithmeticError

    inventory.save()


@receiver(pre_save, sender=Trade)
def check_trade_ok(sender, instance, raw, using, update_fields, **kwargs):
    if instance.source.amount >= instance.amount:
        instance.source.amount -= instance.amount
        instance.source.save()
        instance.destination.amount += instance.amount
        instance.destination.save()
    else:
        raise Exception("Contenu de l'inventaire insuffisant")

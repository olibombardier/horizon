from django.contrib.auth.models import User
from django.test import TestCase

from bank_account.models import BankAccount, Transaction
from character.models import Character, Origin, Skill
from downtime.models import Sector

# Create your tests here.
class BankAccountTestCase(TestCase):
    def setUp(self):
        self.origin = Origin()
        self.origin.save()

        self.user = User()
        self.user.save()

        self.char = Character(origin=self.origin, player=self.user, first_name='first', last_name='last')
        self.char.save()


        self.char2 = Character(origin=self.origin, player=self.user, first_name='first2', last_name='last2')
        self.char2.save()

        self.account1 = self.char.bank_account
        self.account2 = self.char2.bank_account

        # Hacking app will expect this to exist during transactions
        piratage = Skill(
            name="Piratage simple",
            xp_cost=0,
        )
        piratage.save()

    def test_can_transfer_money_to_another_account(self):
        self.account1.balance = 100
        self.account1.save()

        self.assertEqual(self.account1.balance, 100)
        self.assertEqual(self.account2.balance, 0)

        transaction = Transaction(amount=100, source=self.account1, destination=self.account2)
        transaction.save()

        self.assertEqual(self.account1.balance, 0)
        self.assertEqual(self.account2.balance, 100)

    def test_cannot_transfer_more_money_than_owned(self):
        self.account1.balance = 100
        self.account1.save()

        self.assertEqual(self.account1.balance, 100)
        self.assertEqual(self.account2.balance, 0)

        transaction = Transaction(amount=101, source=self.account1, destination=self.account2)
        with self.assertRaises(Exception):
            transaction.save()

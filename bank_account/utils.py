from character.models import Character

from .models import BankAccount

def get_account_owner(account):
    return Character.objects.get(bank_account=account)
    

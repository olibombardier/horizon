from django.contrib import admin

from .models import BankAccount, Transaction

@admin.register(Transaction)
class Transaction(admin.ModelAdmin):
    search_fields = [
        'source__character__first_name', 'source__character__last_name',
        'destination__character__first_name', 'destination__character__last_name',
    ]


@admin.register(BankAccount)
class BankAccount(admin.ModelAdmin):
    search_fields = ['character__first_name', 'character__last_name']

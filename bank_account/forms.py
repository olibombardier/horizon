from django import forms

from bank_account.models import Transaction


class RequestPaymentForm(forms.Form):
    amount = forms.IntegerField(label='Montant demandé')

class ConfirmPaymentForm(forms.Form):
    account_id = forms.IntegerField(widget=forms.widgets.HiddenInput)
    amount_to_provide = forms.IntegerField(widget=forms.widgets.HiddenInput)


class DirectPaymentForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ['amount']

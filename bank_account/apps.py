from django.apps import AppConfig


class BankAccountConfig(AppConfig):
    name = 'bank_account'

    def ready(self):
        import bank_account.signals

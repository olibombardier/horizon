import numpy as np
from django.core.management.base import BaseCommand

from bank_account.models import Transaction
from biomass.models import Biomass, BiomassType
from character.models import Character, Skill, SkillBought
from character.utils import get_generic_account, get_paid_chars_next_larp, pay_character
from program.models import InstalledProgram

from downtime.utils import send_answers
from larpdate.models import LarpDate
from larpdate.utils import get_next_larp


def pay_everyone():
    # Get or create pay character
    chars = get_paid_chars_next_larp()

    for char in chars:
        pay_character(char)
    
def first_biomass_alert():
    # Ensure all Biologists have received their first mother
    biology = Skill.objects.get(name='Bioproducteur')
    biology_bought = SkillBought.objects.filter(skill=biology, character__current_larp_active=True)

    for bio in biology_bought:
        character = bio.character

        if Biomass.objects.filter(owner=character, alive=True).count() == 0:
            biomass_types = BiomassType.objects.exclude(name="Procaryote")
            biomass_type = np.random.choice(biomass_types)

            biomass = Biomass(owner=character, biomass_type=biomass_type)
            biomass.save()


    # Ensure all Biomasses have an alert
    for biomass in Biomass.objects.filter(alive=True, owner__current_larp_active=True):
        biomass.generate_alert(alert_now=True)

def clear_firewalls():
    firewalls = InstalledProgram.objects.filter(
        program__id_code__icontains='PF'
    ).exclude(target__player__is_staff=True)
    for firewall in firewalls:
        firewall.delete()

def activate_characters():
    chars = Character.objects.filter(current_larp_active=True)

    for char in chars:
        char.current_larp_active = False
        char.save()

    to_activate = get_paid_chars_next_larp()
    for char in to_activate:
        char.current_larp_active = True
        char.save()

def activate_larp():
    current_larp = get_next_larp()
    larps = LarpDate.objects.filter(current=True)
    for larp in larps:
        larp.current = False
        larp.save()

    current_larp.current = True
    current_larp.save()

class Command(BaseCommand):
    help = 'Provides Pay and start biomass timers when required'

    def handle(self, *args, **kwargs):
        activate_larp()
        activate_characters()
        pay_everyone()
        first_biomass_alert()
        clear_firewalls()
        send_answers()

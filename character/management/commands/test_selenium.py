from django.core.management.base import BaseCommand
import re
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By


def test_selenium():
    firefox_options = webdriver.FirefoxOptions()
    driver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=firefox_options
    )
    driver.get("https://www.zeffy.com/ticket/b7cd34e7-1949-4bcb-86c8-1fb948bb555f")

    sleep(5)

    body = driver.find_element(By.XPATH, "/html/body").text

    result = re.search("\n(Mégacorp: .*)\n", body).group(1)

    print(result)


class Command(BaseCommand):
    help = 'Parses Horizon website and puts result in skills'

    def handle(self, *args, **kwargs):
        test_selenium()
        # update_contact_skills()

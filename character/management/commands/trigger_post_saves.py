from django.core.management.base import BaseCommand

from character.models import SkillBought


class Command(BaseCommand):
    help = 'Trigger post_saves for all models that may need to do so after a new migration'

    def handle(self, *args, **kwargs):
        chars = SkillBought.objects.all()
        for char in chars:
            char.save()

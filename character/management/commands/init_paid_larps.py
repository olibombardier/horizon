from django.core.management.base import BaseCommand
from character.utils import get_generic_account
from larpdate.models import LarpDate
from character.models import Character
from bank_account.models import Transaction
from datetime import date


def make_larp_paid(transaction):
    try:
        out_date = transaction.datetime.date()
        # Date in extranet is delayed somehow
        if out_date == date(2019, 8, 30):
            out_date = date(2019, 8, 24)
        larpdate = LarpDate.objects.get(out_date=out_date)
        character = transaction.destination.character

        character.paid_larps.add(larpdate)
        character.save()
    except:
        pass


def retrieve_paid_larps():
    paye = get_generic_account('Paye')
    for transaction in Transaction.objects.filter(source=paye.bank_account):
        # Character was in larp
        make_larp_paid(transaction)

    paye = Character.objects.get(
        first_name='Automatique:', last_name='Service de la paie')
    for transaction in Transaction.objects.filter(source=paye.bank_account):
        # Character was in larp
        make_larp_paid(transaction)


class Command(BaseCommand):
    help = 'Parses Horizon website and puts result in skills'

    def handle(self, *args, **kwargs):
        retrieve_paid_larps()
        # update_contact_skills()

from django.contrib import admin

from .models import Character, Corporation, Grade, Origin, Skill, SkillBought

# Register your models here.
admin.site.register(Skill)
admin.site.register(Origin)
admin.site.register(Corporation)
admin.site.register(Grade)

@admin.register(SkillBought)
class SkillBought(admin.ModelAdmin):
    search_fields = ['character__first_name', 'character__last_name', 'skill__name']

@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'last_name']

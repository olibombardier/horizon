from django import forms


class CompleteAlertForm(forms.Form):
    confirmed = forms.BooleanField(widget=forms.widgets.HiddenInput, required=False)

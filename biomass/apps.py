from django.apps import AppConfig


class BiomassConfig(AppConfig):
    name = 'biomass'

    def ready(self):
        import biomass.signals
        from biomass.updater import schedule_due_alerts
        schedule_due_alerts()

from django.contrib.auth import views as auth_views
from django.urls import path

from biomass import views

urlpatterns = [
    path('overview/', views.biomass_overview, name='biomass_overview'),
    path('alert_details/<alert_id>/', views.alert_details, name='alert_details'),
    path('complete_alert/<alert_id>/', views.complete_alert, name='complete_alert'),
]

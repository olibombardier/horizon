import numpy as np
from django.shortcuts import redirect, render

from biomass.forms import CompleteAlertForm
from biomass.models import Alert, Biomass
from character import views as char_views
from character.utils import get_character


# Create your views here.
def biomass_overview(request):
    context = {}
    context['character'] = get_character(request)
    if context['character'].is_locked:
        return redirect(char_views.account_compromised)

    context['biomasses'] = Biomass.objects.filter(owner=context['character'])
    context['alerts'] = Alert.objects.filter(biomass__owner=context['character'], started=True)

    return render(request, 'biomass/overview.djhtml', context)

def alert_details(request, alert_id):
    context={}

    context['character'] = get_character(request)
    if context['character'].is_locked:
        return redirect(char_views.account_compromised)

    context['alert'] = Alert.objects.get(id=alert_id)

    if context['alert'].biomass.owner != context['character']:
        # Fishy stuff
        return redirect(char_views.view_login)

    if context['alert'].completed:
        # Too late!
        return redirect(biomass_overview)

    return render(request, 'biomass/alert_details.djhtml', context)


def complete_alert(request, alert_id):
    context = {}

    context['character'] = get_character(request)
    if context['character'].is_locked:
        return redirect(char_views.account_compromised)

    context['alert'] = Alert.objects.get(id=alert_id)

    if context['alert'].biomass.owner != context['character']:
        # Fishy stuff
        return redirect(char_views.view_login)

    if context['alert'].completed:
        # Too late!
        return redirect(biomass_overview)

    if request.method == 'POST':
        form = CompleteAlertForm(request.POST)
        if form.is_valid():
            alert = context['alert']
            alert.succeeded()

            return redirect(biomass_overview)

    context['form'] = CompleteAlertForm()

    return render(request, 'biomass/complete_alert.djhtml', context)

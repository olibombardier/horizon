import os
import logging

from apscheduler.schedulers.background import BackgroundScheduler
from django.utils import timezone

from biomass.utils import run_alerts


def schedule_due_alerts():
    scheduler = BackgroundScheduler()
    if 'DJANGO_DEBUG_FALSE' in os.environ and 'SSH_CLIENT' not in os.environ:
        scheduler.add_job(run_alerts, 'interval', seconds=10)
    else:
        scheduler.add_job(run_alerts, 'interval', minutes=10)
    scheduler.start()
    logging.getLogger('apscheduler.executors.default').setLevel(logging.WARNING)

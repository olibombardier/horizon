from django.contrib import admin

from biomass.models import Alert, AlertType, Biomass, BiomassType

# Register your models here.
admin.site.register(BiomassType)
admin.site.register(Biomass)
admin.site.register(AlertType)
admin.site.register(Alert)

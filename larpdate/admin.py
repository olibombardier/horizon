from django.contrib import admin

from larpdate.models import LarpDate, ScannedTicket

# Register your models here.
admin.site.register(LarpDate)
admin.site.register(ScannedTicket)

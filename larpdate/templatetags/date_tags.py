from datetime import datetime, timedelta

import pytz
from django import template

from horizon import settings
from larpdate.models import LarpDate

register = template.Library()

@register.simple_tag
def in_date(to_convert):
    larpdates = LarpDate.objects.all().order_by('out_date')
    current = larpdates.first()

    for ilarpdate in larpdates:
        if to_convert.date() >= ilarpdate.out_date:
            current = ilarpdate


    converted = (to_convert + (current.in_date - current.out_date))
    return converted.strftime("%Y-%m-%d %H:%M:%S")

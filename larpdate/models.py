from django.db import models


# Create your models here.
class LarpDate(models.Model):
    out_date = models.DateField()
    in_date = models.DateField()
    end_datetime = models.DateTimeField(null=True)

    current = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.out_date} => {self.in_date}'


class ScannedTicket(models.Model):
    ticket_url = models.TextField()
    character = models.ForeignKey('character.Character', on_delete=models.CASCADE, null=True, related_name='ticket_character')

    def __str__(self):
        return self.ticket_url

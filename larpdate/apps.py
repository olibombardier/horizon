from django.apps import AppConfig


class LarpdateConfig(AppConfig):
    name = 'larpdate'

    def ready(self):
        import larpdate.signals

from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('scan_ticket/', views.scan_ticket, name='scan_ticket'),
    path('check_ticket/<url>', views.check_ticket, name='check_ticket'),
    path('view_ticket_scan_result/<url>', views.view_ticket_scan_result, name='view_ticket_scan_result'),
]

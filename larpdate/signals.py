from django.db.models.signals import post_save
from django.dispatch import receiver

from larpdate.models import LarpDate


@receiver(post_save, sender=LarpDate)
def ensure_only_one_current(sender, instance, created, raw, using, update_fields, **kwargs):
    if instance.current:
        for larpdate in LarpDate.objects.filter(current=True):
            if larpdate != instance:
                larpdate.current = False
                larpdate.save()

from django.db import models

from character.models import Character

# Create your models here.
class Condition(models.Model):
    type = models.CharField(max_length=200)
    is_deadly = models.BooleanField()
    description = models.TextField()
    diagnostic = models.CharField(max_length=200)
    heal_method = models.TextField()

    def __str__(self):
        return self.type

class AppliedCondition(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    condition = models.ForeignKey(Condition, on_delete=models.PROTECT)
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    healed_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        name = f"{self.character} - {self.condition}"
        if self.healed_at:
            name = f"(Soignée) {name}"

        return name

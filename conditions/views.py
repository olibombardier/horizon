from datetime import datetime

from django.shortcuts import redirect, render

from character.utils import get_character

from .models import AppliedCondition

# Create your views here.
def overview(request):
    character = get_character(request)

    context = {}

    context['conditions'] = AppliedCondition.objects.filter(character=character, healed_at=None)
    context['character'] = character

    return render(request, 'conditions/overview.djhtml', context)

def heal_condition(request, condition_id):
    character = get_character(request)

    condition = AppliedCondition.objects.get(id=condition_id)
    if condition.character != character:
        return redirect(overview)
    if condition.character == character:
        condition.healed_at = datetime.now()
        condition.save()

    return redirect(overview)

from django.contrib import admin
from conditions.models import Condition, AppliedCondition

# Register your models here.
admin.site.register(Condition)
admin.site.register(AppliedCondition)

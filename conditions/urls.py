from django.urls import path

from . import views

urlpatterns = [
    path('overview/', views.overview, name='conditions_overview'),
    path('heal_condition/<condition_id>', views.heal_condition, name='heal_condition'),
]
